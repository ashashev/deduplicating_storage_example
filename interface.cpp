/*
 * interface.cpp
 *
 *  Created on: 07 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "interface.h"
#include "factories.h"
#include <memory>

namespace
{

axcient::iserver& server()
{
    static std::unique_ptr<axcient::iserver> server(axcient::create_server());
    return *server;
}

} /* anonymouse namespace */

int init (uint64_t block_size, uint32_t blob_size)
{
    return server().init(block_size, blob_size);
}

int put_block(uint64_t block_id, const byte_t* block_data)
{
    return server().put_block(block_id, block_data);
}

int get_block(uint64_t block_id, byte_t* block_data)
{
    return server().get_block(block_id, block_data);
}

