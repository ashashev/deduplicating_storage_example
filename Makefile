includes = . externals

sources_dirs = . implementation externals externals/md5
sources = $(wildcard $(addsuffix /*.cpp, $(sources_dirs)))
objects = $(notdir $(patsubst %.cpp,%.o,$(sources)))
objects += $(notdir externals/md5/md5.o)

target = libstorage.so

VPATH = $(sources_dirs)

CXXFLAGS = $(addprefix -I, $(includes)) -fPIC -Wall -std=c++14 -fdiagnostics-color=auto

$(target): $(objects)
	$(CXX) $(objects) -shared -o $(target)

.PHONY: clean test all test_lib test_impl

all: $(target) test_impl test_lib

test: test_impl test_lib
	$(MAKE) --directory=implementation/tests run
	$(MAKE) --directory=tests run

test_impl:
	$(MAKE) --directory=implementation/tests

test_lib: $(target)
	$(MAKE) --directory=tests

clean:
	rm -f $(target) $(objects)
	$(MAKE) --directory=tests clean
	$(MAKE) --directory=implementation/tests clean

