/*
 * iserver.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef ISERVER_H_
#define ISERVER_H_

#include "types.h"

namespace axcient
{

struct iserver
{
    /*
     * Метод осуществляет установку соответствующих параметров.
     * block_size — размер передаваемого блока данных в байтах
     * blob_size — размер хранимого blob в количестве блоков
     * Возвращаемые значения: 0 в случае успеха, число отличное от 0 в случае неудачи.
     */
    virtual int init (uint64_t block_size, uint32_t blob_size) = 0;

    /*
     * Метод помещает блок на хранение.
     * block_id — уникальный идентификатор блока
     * block_data —набор байт размером block_size, данные блока
     * Возвращаемые значения: 0 в случае успеха, число отличное от 0 в случае неудачи.
     */
    virtual int put_block(uint64_t block_id, const byte_t* block_data) = 0;

    /*
     * Метод заполняет block_data данными блока block_id.
     * block_id — уникальный идентификатор блока
     * block_data — указатель на свободное место в памяти размером block_size
     */
    virtual int get_block(uint64_t block_id, byte_t* block_data) = 0;

    virtual ~iserver () {}
};

} //namespace axcient

#endif /* ISERVER_H_ */
