/*
 * factories.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "factories.h"

#include "implementation/simple_server.h"
#include "implementation/simple_file_storage.h"
#include "implementation/md5_hash.h"

#include <memory>

axcient::iserver* axcient::create_server()
{
    std::unique_ptr<ihash> hash(new md5_hash);
    std::unique_ptr<istorage> storage(new simple_file_storage);
    return new simple_server(std::move(hash), std::move(storage));
}


