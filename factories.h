/*
 * factories.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef FACTORIES_H_
#define FACTORIES_H_

#include "iserver.h"

namespace axcient
{

iserver* create_server();

}//namespace axcient


#endif /* FACTORIES_H_ */
