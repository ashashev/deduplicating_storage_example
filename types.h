/*
 * types.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <cstdint>
#include <cstddef>

namespace axcient
{

typedef uint8_t byte_t;

}//namespace axcient

#endif /* TYPES_H_ */
