/*
 * simple_file_storage.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_SIMPLE_FILE_STORAGE_H_
#define IMPLEMENTATION_SIMPLE_FILE_STORAGE_H_

#include "implementation/istorage.h"
#include <string>
#include <map>
#include <fstream>

namespace axcient
{

class simple_file_storage: public istorage
{
public:
    typedef std::filebuf filebuf_type;

    simple_file_storage();
    virtual ~simple_file_storage();

    int init(const size_t id_size, const uint64_t block_size, const uint32_t blob_size) override;
    int put_block(const storage_id& id, const byte_t* const block_data) override;
    int get_block(const storage_id& id, byte_t* block_data) override;

    bool is_init() const;
    size_t blocks_count() const;
    uint64_t blobs_count() const;
private:

    bool open_blob_for_writing();
    std::string get_blob_name(const uint64_t blob_id) const;
    int write_block(const storage_id& id, const byte_t* const block_data);
    int read_block(filebuf_type& read_buf, const storage_id& id, byte_t* block_data);

    const std::string _dir_with_blobs;
    const std::string _file_ext;
    size_t _id_size;
    uint64_t _block_size;
    uint32_t _blob_size;
    uint64_t _blob_id;
    bool _is_initialized;
    std::map<storage_id, uint64_t> _id_to_blob;
    filebuf_type _write_buf;
    uint32_t _blocks_written;
};

} /* namespace axcient */

#endif /* IMPLEMENTATION_SIMPLE_FILE_STORAGE_H_ */
