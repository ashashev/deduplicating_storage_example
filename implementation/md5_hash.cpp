/*
 * md5calculator.cpp
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "md5_hash.h"

#include "externals/md5/md5.hh"

#include <memory>

namespace axcient
{

size_t md5_hash::size() const
{
    return _size;
}

raw_data_storage md5_hash::operator()(const byte_t* const data,
        const uint64_t size) const
{
    MD5 md5;
    md5.update(data, size);
    md5.finalize();
    std::unique_ptr<byte_t[]> raw(md5.raw_digest());
    return raw_data_storage(raw.get(), _size);
}

} /* namespace axcient */

