/*
 * simpleserver.h
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_SIMPLE_SERVER_H_
#define IMPLEMENTATION_SIMPLE_SERVER_H_

#include "iserver.h"
#include "ihash.h"
#include "istorage.h"

#include <memory>
#include <map>

namespace axcient
{

class simple_server: public iserver
{
public:
    simple_server(std::unique_ptr<ihash>&& hash,
            std::unique_ptr<istorage>&& storage);
    ~simple_server() override;

    int init (uint64_t block_size, uint32_t blob_size) override;
    int put_block(uint64_t block_id, const byte_t* block_data) override;
    int get_block(uint64_t block_id, byte_t* block_data) override;
private:
    typedef std::map<uint64_t, storage_id> block_to_storage_type;
    std::unique_ptr<ihash> _hash;
    std::unique_ptr<istorage> _storage;
    block_to_storage_type _block_to_storage;
    uint64_t _block_size;
    bool _is_initialized;
};

} /* namespace axcient */

#endif /* IMPLEMENTATION_SIMPLE_SERVER_H_ */
