/*
 * simpleserver.cpp
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "simple_server.h"
#include "return_codes.h"

#include <cassert>

namespace axcient
{

simple_server::simple_server(std::unique_ptr<ihash>&& hash,
        std::unique_ptr<istorage>&& storage) :
                _hash(std::move(hash)),
                _storage(std::move(storage)),
                _block_size(0),
                _is_initialized(false)
{}

simple_server::~simple_server()
{}

int simple_server::init(uint64_t block_size, uint32_t blob_size)
{
    if (!(_hash && _storage))
        return ret_code::general_error;

    if (!(block_size > 0 && blob_size > 0))
        return ret_code::wrong_param;

    if (_is_initialized)
        return ret_code::already_initialized;

    int res = _storage->init(_hash->size(), block_size, blob_size);

    if (res == ret_code::ok)
    {
        _block_size = block_size;
        _is_initialized = true;
    }

    return res;
}

int simple_server::put_block(uint64_t block_id, const byte_t* block_data)
{
    if (!_is_initialized)
        return ret_code::uninitialized;

    if (_block_to_storage.find(block_id) != _block_to_storage.end())
        return ret_code::ok;

    auto stor_id = (*_hash)(block_data, _block_size);
    block_to_storage_type::iterator iter;
    bool inserted;
    std::tie(iter, inserted) = _block_to_storage.emplace(block_id, stor_id);

    assert(inserted);
    if (!inserted)
        return ret_code::general_error;

    int res = _storage->put_block(stor_id, block_data);

    if (res != ret_code::ok)
    {
        _block_to_storage.erase(iter);
        return res;
    }

    return ret_code::ok;
}

int simple_server::get_block(uint64_t block_id, byte_t* block_data)
{
    if (!_is_initialized)
        return ret_code::uninitialized;

    auto iter = _block_to_storage.find(block_id);
    if (iter == _block_to_storage.end())
        return ret_code::block_not_found;

    int res = _storage->get_block(iter->second, block_data);

    if (res != ret_code::ok)
        return res;

    auto hash = (*_hash)(block_data, _block_size);

    if (iter->second != hash)
        return ret_code::corrupted_data;

    return ret_code::ok;
}

} /* namespace axcient */
