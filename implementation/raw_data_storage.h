/*
 * storageid.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_RAW_DATA_STORAGE_H_
#define IMPLEMENTATION_RAW_DATA_STORAGE_H_

#include "types.h"
#include <vector>
#include <string>

namespace axcient
{

class raw_data_storage final
{
public:
    raw_data_storage();
    raw_data_storage(const byte_t * const data, const size_t size);
    raw_data_storage(const raw_data_storage& that);
    raw_data_storage(raw_data_storage&& that);
    raw_data_storage& operator =(const raw_data_storage& that);

    const byte_t* data() const;
    size_t size() const;
private:
    std::vector<byte_t> _data;
};

bool operator ==(const raw_data_storage& lhv, const raw_data_storage& rhv);
bool operator !=(const raw_data_storage& lhv, const raw_data_storage& rhv);
bool operator <(const raw_data_storage& lhv, const raw_data_storage& rhv);
std::string to_hex_string(const axcient::byte_t* const data, const size_t size);

} /* namespace axcient */

#endif /* IMPLEMENTATION_RAW_DATA_STORAGE_H_ */
