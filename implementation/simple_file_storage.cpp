/*
 * simple_file_storage.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "implementation/simple_file_storage.h"
#include "implementation/return_codes.h"

#include <cstdlib>
#include <cstring>
#include <cassert>
#include <memory>

namespace
{

using namespace axcient;

typedef simple_file_storage::filebuf_type filebuf_type;

const filebuf_type::pos_type invalid_pos = filebuf_type::pos_type(
        filebuf_type::off_type(-1));

class reverter_out_position
{
public:
    reverter_out_position(filebuf_type& buffer, filebuf_type::pos_type pos);
    ~reverter_out_position();
    void finish();
private:
    bool _need_revert;
    filebuf_type& _buffer;
    filebuf_type::pos_type _pos;
};

reverter_out_position::reverter_out_position(filebuf_type& buffer,
        filebuf_type::pos_type pos) :
    _need_revert(true),
    _buffer(buffer),
    _pos(pos)
{}

reverter_out_position::~reverter_out_position()
{
    if (_need_revert)
        _buffer.pubseekpos(_pos, std::ios::out);
}

void reverter_out_position::finish()
{
    _need_revert = false;
}

bool write(filebuf_type& buf, const byte_t * data, const uint64_t size)
{
    return buf.sputn(reinterpret_cast<const char*>(data), size) == size;
}

template<typename T>
bool write(filebuf_type& buf, const T value)
{
    return write(buf, reinterpret_cast<const byte_t*>(&value), sizeof(value));
}

bool read(filebuf_type& buf, byte_t * dest, const uint64_t size)
{
    return buf.sgetn(reinterpret_cast<char*>(dest), size) == size;
}

template<typename T>
bool read(filebuf_type& buf, T& value)
{
    return read(buf, reinterpret_cast<byte_t*>(&value), sizeof(T));
}

} /*anonymous namespace*/

namespace axcient
{

simple_file_storage::simple_file_storage() :
        _dir_with_blobs("/tmp/axcient_storage"),
        _file_ext(".dat"),
        _id_size(0),
        _block_size(0),
        _blob_size(0),
        _blob_id(0),
        _blocks_written(0),
        _is_initialized(false)
{

}

simple_file_storage::~simple_file_storage()
{
    if (_write_buf.is_open())
        _write_buf.close();
}

int simple_file_storage::init(const size_t id_size, const uint64_t block_size,
        const uint32_t blob_size)
{
    if (!(id_size > 0 && block_size > 0 && blob_size > 0))
        return ret_code::wrong_param;

    if (std::system(("rm -rf " + _dir_with_blobs).c_str()) != 0)
        return ret_code::general_error;

    if (std::system(("mkdir -p " + _dir_with_blobs).c_str()) != 0)
        return ret_code::general_error;

    _id_size = id_size;
    _block_size = block_size;
    _blob_size = blob_size;
    _is_initialized = true;

    return ret_code::ok;
}

int simple_file_storage::put_block(const storage_id& id,
        const byte_t* const block_data)
{
    if (!_is_initialized)
        return ret_code::uninitialized;

    assert(id.size() == _id_size);

    if (_id_to_blob.find(id) != _id_to_blob.end())
    {
        return ret_code::ok;
    }
    else
    {
        if (!open_blob_for_writing())
            return ret_code::preparing_to_write_failed;
    }

    return write_block(id, block_data);
}

int simple_file_storage::get_block(const storage_id& id, byte_t* block_data)
{
    if (!_is_initialized)
        return ret_code::uninitialized;

    assert(id.size() == _id_size);
    if (id.size() != _id_size)
        return ret_code::wrong_param;

    auto id_to_blob_iter = _id_to_blob.find(id);
    if (id_to_blob_iter != _id_to_blob.end())
    {
        if (id_to_blob_iter->second == _blob_id)
        {
            return read_block(_write_buf, id, block_data);
        }
        else
        {
            using namespace std;

            filebuf_type read_buf;
            read_buf.open(get_blob_name(id_to_blob_iter->second),
                    ios::binary | ios::in);
            if (!read_buf.is_open())
                return ret_code::preparing_to_read_failed;

            return read_block(read_buf, id, block_data);
        }
    }

    return ret_code::block_not_found;
}

bool simple_file_storage::is_init() const
{
    return _is_initialized;
}

size_t simple_file_storage::blocks_count() const
{
    return _id_to_blob.size();
}

uint64_t simple_file_storage::blobs_count() const
{
    return _blob_id + (_write_buf.is_open()? 1: 0);
}

bool simple_file_storage::open_blob_for_writing()
{
    using namespace std;
    if (!_write_buf.is_open())
    {
        if (!_write_buf.open(
                get_blob_name(_blob_id),
                ios::binary | ios::out | ios::in | ios::app)
                )
        {
            return false;
        }
    }
    else if (_blocks_written == _blob_size)
    {
        _write_buf.close();
        ++_blob_id;
        _blocks_written = 0;
        return open_blob_for_writing();
    }
    return true;
}

std::string simple_file_storage::get_blob_name(const uint64_t blob_id) const
{
    return _dir_with_blobs + "/" + std::to_string(blob_id) + _file_ext;
}

int simple_file_storage::write_block(const storage_id& id,
        const byte_t* const block_data)
{
    filebuf_type::pos_type cur = _write_buf.pubseekoff(
                                        0,
                                        std::ios::cur,
                                        std::ios::out);
    if (cur == invalid_pos)
        return ret_code::general_error;

    reverter_out_position reverter(_write_buf, cur);

    assert(_id_size == id.size());

    if (!write(_write_buf, _id_size))
        return ret_code::writing_failed;

    if (!write(_write_buf, id.data(), id.size()))
        return ret_code::writing_failed;

    if (!write(_write_buf, _block_size))
        return ret_code::writing_failed;

    if (!write(_write_buf, block_data, _block_size))
        return ret_code::writing_failed;

    if (!_id_to_blob.emplace(id, _blob_id).second)
        return ret_code::general_error;

    ++_blocks_written;

    reverter.finish();

    return ret_code::ok;
}

int simple_file_storage::read_block(filebuf_type& read_buf,
        const storage_id& id, byte_t* block_data)
{
    using namespace std;

    assert(read_buf.is_open());

    std::unique_ptr<byte_t[]> id_buf(new byte_t[_id_size]);
    decltype(_id_size) id_size = 0;
    decltype(_block_size) block_size = 0;

    if (read_buf.pubseekoff(0, ios::beg, ios::in) == invalid_pos)
        return ret_code::general_error;

    while(read_buf.in_avail() > 0)
    {
        if (!read(read_buf, id_size) || (id_size != _id_size))
            return ret_code::reading_failed;

        if (!read(read_buf, id_buf.get(), id_size))
            return ret_code::reading_failed;

        if (!read(read_buf, block_size) || (block_size != _block_size))
            return ret_code::reading_failed;

        if (std::memcmp(id.data(), id_buf.get(), id_size) == 0)
            return read(read_buf, block_data, block_size)?
                    ret_code::ok:
                    ret_code::reading_failed;
        else
            if (read_buf.pubseekoff(block_size, ios::cur, ios::in) == invalid_pos)
                return ret_code::general_error;
    }

    return ret_code::block_not_found;
}

} /* namespace axcient */

