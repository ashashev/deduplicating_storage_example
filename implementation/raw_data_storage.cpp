/*
 * storageid.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "implementation/raw_data_storage.h"

#include <cstring>
#include <sstream>
#include <iomanip>

namespace axcient
{

raw_data_storage::raw_data_storage()
{

}

raw_data_storage::raw_data_storage(const byte_t* const data, const size_t size) :
        _data(size)
{
    std::memcpy(_data.data(), data, size);
}

raw_data_storage::raw_data_storage(const raw_data_storage& that) :
        _data(that._data)
{
}

raw_data_storage::raw_data_storage(raw_data_storage&& that) :
        _data(std::move(that._data))
{
}

raw_data_storage& raw_data_storage::operator =(const raw_data_storage& that)
{
    _data = that._data;
    return *this;
}

const byte_t* raw_data_storage::data() const
{
    return _data.data();
}

size_t raw_data_storage::size() const
{
    return _data.size();
}

} /* namespace axcient */

bool axcient::operator ==(const raw_data_storage& lhv, const raw_data_storage& rhv)
{
    if (lhv.size() != rhv.size())
        return false;
    return std::memcmp(lhv.data(), rhv.data(), lhv.size()) == 0;
}

bool axcient::operator !=(const raw_data_storage& lhv, const raw_data_storage& rhv)
{
    return !(lhv == rhv);
}

bool axcient::operator <(const raw_data_storage& lhv, const raw_data_storage& rhv)
{
    if (lhv.size() < rhv.size())
        return true;
    return std::memcmp(lhv.data(), rhv.data(), lhv.size()) < 0;
}

std::string axcient::to_hex_string(const axcient::byte_t* const data, const size_t size)
{
    using namespace std;
    ostringstream os;
    os << hex << setfill('0');
    for (size_t i = 0; i < size; ++i)
        os << setw(2) << static_cast<unsigned int>(data[i]);
    return os.str();
}
