/*
 * tests_md5_calculator_suite.cpp
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "implementation/md5_hash.h"

#include "auxiliary_functions.h"

#include <list>
#include <string>
#include <utility>
#include <cassert>
#include <cstring>

#include "catch/catch.hpp"

using namespace axcient;

TEST_CASE("Test MD5 hash Suite", "[md5_calculator]")
{
    const std::list< std::pair<std::string, std::string> > test_list = {
            { "Hello, World!", "65a8e27d8879283831b664bd8b7f0ad4" },
            { "MD5 (Message-Digest algorithm 5)", "03d603d099a405e7afa5e6cc382d8843" },
            { "finding a collision with some pre-existing data is a much more difficult problem", "c6e559f9e6e1439191979f9cd86f0258" }
        };

    md5_hash hash;

    for (auto& test: test_list )
    {
        auto res = hash(
                reinterpret_cast<const byte_t*>(test.first.c_str()),
                test.first.length()
                );
        INFO("data: " << test.first);
        REQUIRE(res.size() == hash.size());
        REQUIRE(res == hex_str_to_raw(test.second));
    }
}

