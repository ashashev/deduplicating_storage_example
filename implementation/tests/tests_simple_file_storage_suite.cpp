/*
 * tests_simple_file_storage_suite.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "implementation/simple_file_storage.h"
#include "implementation/return_codes.h"

#include "catch/catch.hpp"
#include "md5/md5.hh"

#include "data_block.h"

#include <cstdlib>
#include <cstring>
#include <vector>
#include <memory>
#include <map>
#include <ctime>

using namespace axcient;

namespace
{

static const size_t hash_size = 16;

typedef std::unique_ptr<byte_t[]> hash_type;

storage_id calc_hash(const byte_t* const data, const uint64_t len)
{
    MD5 md5;
    md5.update(data, len);
    md5.finalize();
    std::unique_ptr<byte_t[]> raw(md5.raw_digest());
    return storage_id(raw.get(), hash_size);
}

} /*anonymous namespace*/

TEST_CASE("Simple File Storage Base Suite", "[simple_file_storage]")
{
    const size_t block_size = 128;
    const size_t blocks_per_blob = 2;

    simple_file_storage storage;
    REQUIRE_FALSE(storage.is_init());

    SECTION("Wrong initialization")
    {
        REQUIRE(storage.init(0, block_size, blocks_per_blob) ==
                ret_code::wrong_param);
        REQUIRE(storage.init(hash_size, 0, blocks_per_blob) ==
                ret_code::wrong_param);
        REQUIRE(storage.init(hash_size, block_size, 0) ==
                ret_code::wrong_param);
    }

    SECTION("Try to work without initialization")
    {
        std::srand(0);
        data_block block(block_size);
        auto hash = calc_hash(block.data(), block.size());
        REQUIRE(storage.put_block(hash, block.data()) ==
                ret_code::uninitialized);
        std::unique_ptr<byte_t[]> buf(new byte_t[block_size]);
        REQUIRE(storage.get_block(hash, buf.get()) ==
                ret_code::uninitialized);
    }

    SECTION("Try to get not existed block")
    {
        REQUIRE(storage.init(hash_size, block_size, blocks_per_blob) == ret_code::ok);
        REQUIRE(storage.is_init());

        std::srand(0);
        data_block block1(block_size);
        auto hash1 = calc_hash(block1.data(), block1.size());
        data_block block2(block_size);
        auto hash2 = calc_hash(block2.data(), block2.size());

        REQUIRE(storage.put_block(hash1, block1.data()) ==
                ret_code::ok);

        std::unique_ptr<byte_t[]> buf(new byte_t[block_size]);
        REQUIRE(storage.get_block(hash2, buf.get()) ==
                ret_code::block_not_found);
    }

    SECTION("Normal work")
    {
        REQUIRE(storage.init(hash_size, block_size, blocks_per_blob) == ret_code::ok);
        REQUIRE(storage.is_init());

        std::time_t generator_seed = std::time(0);
        INFO("generator_seed " << generator_seed);
        std::srand(generator_seed);

        std::vector<data_block> blocks;
        std::map<size_t, decltype(calc_hash(nullptr, 0))> id_to_hash;

        blocks.emplace_back(block_size); //0
        blocks.emplace_back(block_size); //1
        blocks.emplace_back(block_size); //2
        blocks.emplace_back(blocks[0]);  //3
        blocks.emplace_back(blocks[1]);  //4

        REQUIRE(blocks[3] == blocks[0]);
        REQUIRE(blocks[4] == blocks[1]);

        for(size_t i = 0; i < blocks.size(); ++i)
        {
            INFO("block number is " << i);
            auto res = id_to_hash.emplace(i, calc_hash(blocks[i].data(),
                    blocks[i].size()));
            REQUIRE(res.second);
            REQUIRE(storage.put_block(res.first->second, blocks[i].data()) ==
                    ret_code::ok);
        }

        {
            INFO("blocks count is " << blocks.size() <<
                    "\nblob size is " << blocks_per_blob <<
                    "\nbut blocks[0] == blocks[3] and blocks[1] == blocks[4]");
            REQUIRE(storage.blocks_count() == blocks.size() - 2);
            REQUIRE(storage.blobs_count() == 2);
        }

        for(size_t i = 0; i < blocks.size(); ++i)
        {
            INFO("block number is " << i);
            std::unique_ptr<byte_t[]> read(new byte_t[block_size]);
            REQUIRE(storage.get_block(id_to_hash[i], read.get()) == ret_code::ok);
        }
    }
}
