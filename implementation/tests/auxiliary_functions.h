/*
 * auxiliary_functions.h
 *
 *  Created on: 06 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef TESTS_AUXILIARY_FUNCTIONS_H_
#define TESTS_AUXILIARY_FUNCTIONS_H_

#include "implementation/raw_data_storage.h"

#include <string>
#include <ostream>

axcient::raw_data_storage hex_str_to_raw(const std::string& str);

namespace axcient
{

std::ostream& operator << (std::ostream& os, const raw_data_storage& data);

}


#endif /* TESTS_AUXILIARY_FUNCTIONS_H_ */
