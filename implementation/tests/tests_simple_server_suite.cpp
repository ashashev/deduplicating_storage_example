/*
 * tests_simple_server_suite.cpp
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "implementation/simple_server.h"
#include "implementation/md5_hash.h"
#include "implementation/return_codes.h"

#include "data_block.h"
#include "auxiliary_functions.h"

#include "externals/catch/catch.hpp"

#include <memory>

using namespace axcient;

namespace
{

class mock_hash_counter
{
public:
    mock_hash_counter() :
        _destructor(0),
        _hash(hex_str_to_raw("aa"))
        { reset(); }
    void reset() { _size = _operator_parenthesis = 0; }
    void size_inc() { ++_size; }
    void operator_parenthesis_inc() { ++_operator_parenthesis; }
    void destructor_inc() { ++_destructor; }
    unsigned size() { return _size; }
    unsigned operator_parenthesis() { return _operator_parenthesis; }
    unsigned destructor() { return _destructor; }
    raw_data_storage hash() { return _hash; }
    void hash(const raw_data_storage& value) { _hash = value; }
    unsigned hash_size() { return _hash.size(); }
private:
    unsigned _size;
    unsigned _operator_parenthesis;
    unsigned _destructor;
    raw_data_storage _hash;
};

class mock_hash: public axcient::ihash
{
public:
    mock_hash(mock_hash_counter& counter);
    ~mock_hash();
    size_t size() const override;
    raw_data_storage operator()(const byte_t* const data,
            const uint64_t size) const override;

    unsigned calling_size() const;
    unsigned calling_oper_parenthesis() const;
    void reset_calling();
private:
    mock_hash_counter& _counter;
};

mock_hash::mock_hash(mock_hash_counter& counter) :
        _counter(counter)
{}

mock_hash::~mock_hash()
{
    _counter.destructor_inc();
}

size_t mock_hash::size() const
{
    _counter.size_inc();
    return _counter.hash_size();
}

raw_data_storage mock_hash::operator ()(const byte_t* const data,
        const uint64_t size) const
{
    _counter.operator_parenthesis_inc();
    return _counter.hash();
}

//////////////////////////////////////////////////////////////////////////////

class mock_storage_counter
{
public:
    mock_storage_counter() : _destructor(0), _returning(0) { reset(); };
    void reset() { _init = _put_block = _get_block = 0; }
    void init_inc() { ++_init; }
    void put_block_inc() { ++_put_block; }
    void get_block_inc() { ++_get_block; }
    void destructor_inc() { ++_destructor; }
    unsigned init() { return _init; }
    unsigned put_block() { return _put_block; }
    unsigned get_block() { return _get_block; }
    unsigned destructor() { return _destructor; }
    void returning(int value) { _returning = value; }
    int returning() const { return _returning; }
private:
    unsigned _init;
    unsigned _put_block;
    unsigned _get_block;
    unsigned _destructor;
    int _returning;
};

class mock_storage : public istorage
{
public:
    mock_storage(mock_storage_counter& counter);
    ~mock_storage();
    int init(const size_t id_size, const uint64_t block_size,
            const uint32_t blob_size) override;
    int put_block(const storage_id& id, const byte_t* const block_data) override;
    int get_block(const storage_id& id, byte_t* block_data) override;
private:
    mock_storage_counter& _counter;
};


mock_storage::mock_storage(mock_storage_counter& counter) :
        _counter(counter)
{}

mock_storage::~mock_storage()
{
    _counter.destructor_inc();
}

int mock_storage::init(const size_t id_size, const uint64_t block_size,
        const uint32_t blob_size)
{
    _counter.init_inc();
    return _counter.returning();
}

int mock_storage::put_block(const storage_id& id, const byte_t* const block_data)
{
    _counter.put_block_inc();
    return _counter.returning();
}

int mock_storage::get_block(const storage_id& id, byte_t* block_data)
{
    _counter.get_block_inc();
    return _counter.returning();
}

} /* anonymouse namespace */

TEST_CASE("Test simple_server Suite", "[simple_server]")
{
    const size_t block_size = 128;
    const size_t blocks_per_blob = 2;

    std::srand(0);

    mock_hash_counter hash_counter;
    mock_storage_counter storage_counter;

    std::unique_ptr<simple_server> server(new simple_server(
                std::unique_ptr<ihash>(new mock_hash(hash_counter)),
                std::unique_ptr<istorage>(new mock_storage(storage_counter))
            ));

    REQUIRE(hash_counter.destructor() == 0);
    REQUIRE(hash_counter.size() == 0);
    REQUIRE(hash_counter.operator_parenthesis() == 0);

    REQUIRE(storage_counter.destructor() == 0);
    REQUIRE(storage_counter.init() == 0);
    REQUIRE(storage_counter.get_block() == 0);
    REQUIRE(storage_counter.put_block() == 0);

    std::unique_ptr<byte_t[]> buf(new byte_t[block_size]);

    SECTION("Try to use uninitialized server")
    {
        data_block block(block_size);

        REQUIRE(server->put_block(0, block.data()) == ret_code::uninitialized);
        REQUIRE(hash_counter.operator_parenthesis() == 0);
        REQUIRE(storage_counter.put_block() == 0);

        REQUIRE(server->get_block(0, buf.get()) == ret_code::uninitialized);
        REQUIRE(hash_counter.operator_parenthesis() == 0);
        REQUIRE(storage_counter.get_block() == 0);
    }

    SECTION("Failed initialization")
    {
        data_block block(block_size);

        storage_counter.returning(0xF0F0F0);
        REQUIRE(server->init(block_size, blocks_per_blob) ==
                storage_counter.returning());

        REQUIRE(server->init(0, blocks_per_blob) == ret_code::wrong_param);
        REQUIRE(server->init(block_size, 0) == ret_code::wrong_param);

        REQUIRE(hash_counter.size() == 1);
        REQUIRE(storage_counter.init() == 1);

        REQUIRE(server->put_block(0, block.data()) == ret_code::uninitialized);
        REQUIRE(hash_counter.operator_parenthesis() == 0);
        REQUIRE(storage_counter.put_block() == 0);

        REQUIRE(server->get_block(0, buf.get()) == ret_code::uninitialized);
        REQUIRE(hash_counter.operator_parenthesis() == 0);
        REQUIRE(storage_counter.get_block() == 0);
    }

    SECTION("Right initialization")
    {
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);
        REQUIRE(hash_counter.size() == 1);
    }

    SECTION("Reading non existed block")
    {
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        REQUIRE(server->get_block(0, buf.get()) == ret_code::block_not_found);
        REQUIRE(hash_counter.operator_parenthesis() == 0);
        REQUIRE(storage_counter.get_block() == 0);
    }

    SECTION("Write error")
    {
        data_block block(block_size);
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        storage_counter.returning(0xF0F0F0);
        REQUIRE(server->put_block(0, block.data()) ==
                storage_counter.returning());
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);

        storage_counter.returning(0);
        REQUIRE(server->get_block(0, buf.get()) == ret_code::block_not_found);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.get_block() == 0);
    }

    SECTION("Write two identical blocks with same id")
    {
        data_block block(block_size);
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        REQUIRE(server->put_block(0, block.data()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);

        REQUIRE(server->put_block(0, block.data()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);
    }

    SECTION("Read error")
    {
        data_block block(block_size);
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        REQUIRE(server->put_block(0, block.data()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);

        storage_counter.returning(0xF0F0F0);
        REQUIRE(server->get_block(0, buf.get()) ==
                storage_counter.returning());
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.get_block() == 1);
    }

    SECTION("Normal work")
    {
        data_block block(block_size);
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        REQUIRE(server->put_block(0, block.data()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);

        REQUIRE(server->get_block(0, buf.get()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 2);
        REQUIRE(storage_counter.get_block() == 1);
    }

    SECTION("Wrong hash for read data")
    {
        data_block block(block_size);
        REQUIRE(server->init(block_size, blocks_per_blob) == ret_code::ok);

        uint64_t block_id = 5;

        hash_counter.hash(hex_str_to_raw("a1"));
        REQUIRE(server->put_block(block_id, block.data()) == ret_code::ok);
        REQUIRE(hash_counter.operator_parenthesis() == 1);
        REQUIRE(storage_counter.put_block() == 1);

        hash_counter.hash(hex_str_to_raw("a2"));
        REQUIRE(server->get_block(block_id, buf.get()) == ret_code::corrupted_data);
        REQUIRE(hash_counter.operator_parenthesis() == 2);
        REQUIRE(storage_counter.get_block() == 1);
    }

    server.reset();

    REQUIRE(hash_counter.destructor() == 1);
    REQUIRE(storage_counter.destructor() == 1);
}

