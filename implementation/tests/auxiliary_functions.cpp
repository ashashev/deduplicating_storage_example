/*
 * auxiliary_functions.cpp
 *
 *  Created on: 06 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "auxiliary_functions.h"

#include "catch/catch.hpp"

#include <cstring>

axcient::raw_data_storage hex_str_to_raw(const std::string& str)
{
    using namespace axcient;

    INFO(str);
    REQUIRE(str.length() % 2 == 0);
    const size_t raw_size = str.length() / 2;
    byte_t raw[raw_size];

    for(size_t p = 0; p < str.length(); p = p + 2)
    {
        size_t size = 0;
        raw[p / 2] = std::stoi(str.substr(p, 2), &size, 16);
        REQUIRE(size == 2);
    }

    return raw_data_storage(raw, raw_size);
}

std::ostream& axcient::operator << (std::ostream& os,
        const raw_data_storage& data)
{
    os << to_hex_string(data.data(), data.size());
    return os;
}

TEST_CASE("Test auxiliary function hex_str_to_raw", "[aux]")
{
    using namespace axcient;

    auto hash_calc = hex_str_to_raw("65a8e27d8879283831b664bd8b7f0ad4");
    const byte_t raw_excepted[] = {
            0x65, 0xa8, 0xe2, 0x7d, 0x88, 0x79, 0x28, 0x38,
            0x31, 0xb6, 0x64, 0xbd, 0x8b, 0x7f, 0x0a, 0xd4 };
    REQUIRE(sizeof(raw_excepted) == 16);
    REQUIRE(std::memcmp(hash_calc.data(), raw_excepted, 16) == 0);
}

