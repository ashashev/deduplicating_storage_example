/*
 * ihash_calculator.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_IHASH_H_
#define IMPLEMENTATION_IHASH_H_

#include "types.h"
#include "raw_data_storage.h"

namespace axcient
{

struct ihash
{
    virtual size_t size() const = 0;
    virtual raw_data_storage operator()(const byte_t* const data,
            const uint64_t size) const = 0;
    virtual ~ihash() {}
};

} /* namespace axcient */

#endif /* IMPLEMENTATION_IHASH_H_ */
