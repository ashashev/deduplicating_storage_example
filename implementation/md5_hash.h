/*
 * md5calculator.h
 *
 *  Created on: 05 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef MD5CALCULATOR_H_
#define MD5CALCULATOR_H_

#include "ihash.h"

namespace axcient
{

class md5_hash: public ihash
{
public:
    size_t size() const override;
    raw_data_storage operator()(const byte_t* const data,
            const uint64_t size) const override;
private:
    const static size_t _size = 16;
};

} /* namespace axcient */

#endif /* MD5CALCULATOR_H_ */
