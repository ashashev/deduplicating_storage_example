/*
 * return_codes.h
 *
 *  Created on: 06 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_RETURN_CODES_H_
#define IMPLEMENTATION_RETURN_CODES_H_


namespace axcient
{

namespace ret_code
{

enum ret_code_value : int {
    ok = 0,
    general_error,
    wrong_param,
    already_initialized,
    initializing_failed,
    uninitialized,
    block_not_found,
    corrupted_data,
    preparing_to_write_failed,
    preparing_to_read_failed,
    writing_failed,
    reading_failed
};

} /* namespace ret_code */

} /* namespace axcient */


#endif /* IMPLEMENTATION_RETURN_CODES_H_ */
