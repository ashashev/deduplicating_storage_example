/*
 * istorage.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef IMPLEMENTATION_ISTORAGE_H_
#define IMPLEMENTATION_ISTORAGE_H_

#include "types.h"
#include "raw_data_storage.h"

namespace axcient
{

typedef raw_data_storage storage_id;

struct istorage
{
    virtual int init(const size_t id_size, const uint64_t block_size,
            const uint32_t blob_size) = 0;
    virtual int put_block(const storage_id& id,
            const byte_t* const block_data) = 0;
    virtual int get_block(const storage_id& id, byte_t* block_data) = 0;
    virtual ~istorage() {};
};

}

#endif /* IMPLEMENTATION_ISTORAGE_H_ */
