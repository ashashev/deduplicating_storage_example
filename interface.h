/*
 * interface.h
 *
 *  Created on: 07 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "types.h"

extern "C"
{

typedef axcient::byte_t byte_t;

/*
 * Метод осуществляет установку соответствующих параметров.
 * block_size — размер передаваемого блока данных в байтах
 * blob_size — размер хранимого blob в количестве блоков
 * Возвращаемые значения: 0 в случае успеха, число отличное от 0 в случае неудачи.
 */
int init (uint64_t block_size, uint32_t blob_size);

/*
 * Метод помещает блок на хранение.
 * block_id — уникальный идентификатор блока
 * block_data —набор байт размером block_size, данные блока
 * Возвращаемые значения: 0 в случае успеха, число отличное от 0 в случае неудачи.
 */
int put_block(uint64_t block_id, const byte_t* block_data);

/*
 * Метод заполняет block_data данными блока block_id.
 * block_id — уникальный идентификатор блока
 * block_data — указатель на свободное место в памяти размером block_size
 */
int get_block(uint64_t block_id, byte_t* block_data);

} /*extern "C"*/

#endif //!__INTERFACE_H__
