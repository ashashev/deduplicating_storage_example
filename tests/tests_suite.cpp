/*
 * tests_suite.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "interface.h"

#include "data_block.h"

#include <memory>
#include <cstring>

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

TEST_CASE("Library test suite")
{
    const size_t block_size = 128;
    const size_t blocks_per_blob = 2;

    REQUIRE(init(block_size, blocks_per_blob) == 0);

    std::time_t generator_seed = std::time(0);
    INFO("generator_seed " << generator_seed);
    std::srand(generator_seed);

    std::vector<data_block> blocks;

    blocks.emplace_back(block_size); //0
    blocks.emplace_back(block_size); //1
    blocks.emplace_back(block_size); //2
    blocks.emplace_back(blocks[0]);  //3
    blocks.emplace_back(blocks[1]);  //4

    REQUIRE(blocks[3] == blocks[0]);
    REQUIRE(blocks[4] == blocks[1]);

    for(size_t i = 0; i < blocks.size(); ++i)
    {
        INFO("put block " << i);
        REQUIRE(put_block(i, blocks[i].data()) == 0);
    }

    for(size_t i = 0; i < blocks.size(); ++i)
    {
        INFO("get block forward" << i);
        std::unique_ptr<byte_t[]> buf(new byte_t[block_size]);
        REQUIRE(get_block(i, buf.get()) == 0);
        REQUIRE(std::memcmp(blocks[i].data(), buf.get(), block_size) == 0);
    }

    for(size_t i = blocks.size() - 1; i; i--)
    {
        INFO("get block inverse" << i);
        std::unique_ptr<byte_t[]> buf(new byte_t[block_size]);
        REQUIRE(get_block(i, buf.get()) == 0);
        REQUIRE(std::memcmp(blocks[i].data(), buf.get(), block_size) == 0);
    }
}

