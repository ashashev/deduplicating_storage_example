/*
 * data_block.cpp
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#include "data_block.h"

#include <cstdlib>
#include <cstring>

using namespace axcient;

data_block::data_block(const size_t size) :
        _data(size)
{
    for (auto& b: _data)
        b = std::rand() % 0xFF;
}

data_block::data_block(data_block&& that) :
        _data(std::move(that._data))
{
}

data_block::data_block(const data_block& that) :
        _data(that._data)
{
}

const axcient::byte_t* data_block::data() const
{
    return _data.data();
}

size_t data_block::size() const
{
    return _data.size();
}

bool operator ==(const data_block& lhv, const data_block& rhv)
{
    if (lhv.size() != rhv.size())
        return false;
    return std::memcmp(lhv.data(), rhv.data(), lhv.size()) == 0;
}
