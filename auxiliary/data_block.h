/*
 * data_block.h
 *
 *  Created on: 04 февр. 2016 г.
 *      Author: Alexey Shashev
 */

#ifndef TESTS_DATA_BLOCK_H_
#define TESTS_DATA_BLOCK_H_

#include "types.h"
#include <vector>
#include <string>

class data_block final
{
public:
    data_block(const size_t size);
    data_block(data_block&& that);
    data_block(const data_block& that);

    const axcient::byte_t* data() const;
    size_t size() const;
private:
    std::vector<axcient::byte_t> _data;
};

bool operator ==(const data_block& lhv, const data_block& rhv);

#endif /* TESTS_DATA_BLOCK_H_ */
